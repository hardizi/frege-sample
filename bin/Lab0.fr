module Lab0 where

data Lab0 = Lab0


evens1 :: [Integer] -> [Integer]
evens1 l1 = [z | z <- l1, 0 == mod z 2] 


squares1 :: Integer -> [Integer] 

squares1 0 = []
squares1 i = [j*j | j <- [1..i]]  

squares' :: Integer -> Integer -> [Integer] 

squares' 0 m = []
squares' n m = [j*j | j <- [m+1..n+m]]  


sumSquares n = sum (squares1 n)

sumSquares' x = sum . uncurry squares' $ (x, x)

coords :: Integer -> Integer -> [(Integer, Integer)]
coords m n = [(x,y) | x <-[0..m] , y <-[0..n] ]

x :: Integer
y :: Integer  
x = 7

y = 8

main args = do
	println (evens1 [2,1,4])
	println (sum . evens1 $ [827305 .. 927104])
	println (sum . evens1 $ [])
	println (sumSquares 50)
	println (sumSquares' 50)
	println (sum $ squares' 10 0)
	println (coords 1 2)
	println (foldr (-) 0 . map (uncurry (*)) $ coords 5 7)
	println(x ^ 80)
