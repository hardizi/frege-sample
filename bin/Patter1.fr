module Patter1 where

data Patter1 = Patter1

data Customer = Customer {
      customerID      :: Integer
    , customerName    :: String
    , customerAddress :: String
    } 
derive  Show Customer


myNot True = False
myNot _ = True 

sumList1 [] = 0
sumList1 (x:xs) = x + sumList1 xs

premier (a,b,c)  = a

complicated (True, a, x:xs, 5) = (a, xs)
complicated (_, _, a, b) = (b, a)

customer1 = Customer 1 "Name" "Adress"

customer2 = Customer {
              customerID = 271828
            , customerAddress = "Adress 2"
            , customerName = "Name 2"
            }

-- Report Error
mySecond :: [a] -> a

mySecond xs = if null (tail xs)
              then error "list too short"
              else head (tail xs)
              

-- More controlled version              
safeSecond :: [a] -> Maybe a

safeSecond [] = Nothing
safeSecond xs = if null (tail xs)
                then Nothing
                else Just (head (tail xs))
                
--Introducing local variables

lend amount balance = let reserve    = 100
                          newBalance = balance - amount
                      in if balance < reserve
                         then Nothing
                         else Just newBalance
                         
--Shadowing : multiple let
foo = let a = 1
      in let b = 2
         in a + b


bar = let x = 1
      in ((let x = "foo" in x), x)
      
-- We can also shadow a function's parameters
quux a = let a = "foo"
       in a ++ "eek!"


--The where clause

lend2 amount balance = if amount < reserve * 0.5
                       then Just newBalance
                       else Nothing
    where reserve    = 100
          newBalance = balance - amount
          
--Local functions
pluralise :: String -> [Int] -> [String]
pluralise word counts = map plural counts
    where plural 0 = "no " ++ word ++ "s"
          plural 1 = "one " ++ word
          plural n = show n ++ " " ++ word ++ "s"
          
--The case expression : The case construct lets us match patterns within an expression
fromMaybe defval wrapped =
    case wrapped of
      Nothing     -> defval
      Just value  -> value

--Conditional evaluation with guards

data Tree a = Node a (Tree a) (Tree a) | Empty
--derive Show Tree

nodesAreSame (Node a _ _) (Node b _ _)
    | a == b     = Just a
nodesAreSame _ _ = Nothing

lend3 amount balance
     | amount <= 0            = Nothing
     | amount > reserve * 0.5 = Nothing
     | otherwise              = Just newBalance
    where reserve    = 100
          newBalance = balance - amount
          
          
niceDrop n xs | n <= 0 = xs
niceDrop _ []          = []
niceDrop n (_:xs)      = niceDrop (n - 1) xs




main args = do
 println (myNot True )
 println (premier (10,2,3) )
 println (complicated (False, 5, [7], 5) )
 println customer1
 println customer2
 println (tail [3]) 
