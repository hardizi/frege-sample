module Datas where

--import util.Assertions(assert)

data Datas = Datas

data BookInfo = Book Int String [String]     
deriving Show BookInfo


-- type synonyms
type CardHolder = String
type CardNumber = String
type Address    = [String]
type CustomerID = Int
-- multi-constructor data types
data BillingInfo = CreditCard CardNumber CardHolder Address
                 | CashOnDelivery
                 | Invoice CustomerID
derive Show BillingInfo


-- enums are just data types with many parameterless constructors
data MyColor = Red
             | Green
             | Blue
derive Eq   MyColor
derive Show MyColor


myInfo = Book 1 "Algebra of Programming" ["Richard Bird", "Oege de Moor1"]

billingInfo = CreditCard "2901650221064486" "Thomas Gradgrind" ["Dickens", "England1"]




main args = do
	println myInfo;
    println billingInfo
         